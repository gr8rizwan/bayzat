<?php

namespace AppBundle\Controller;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;
use AppBundle\Entity\Car;

class CarController extends FOSRestController
{
    /**
     * @Rest\Get("/cars")
     */
    public function getAction()
    {
        $restresult = $this->getDoctrine()->getRepository('AppBundle:Car')->findAll();
        if ($restresult === null || empty($restresult)) {
            $response = [
                'status' => false,
                'message' => "there are no cars exist"
            ];
            return new View($response, Response::HTTP_NOT_FOUND);
        }
        return $restresult;
    }

    /**
     * @Rest\Get("/car/{id}")
     */
    public function idAction($id)
    {
        $singleresult = $this->getDoctrine()->getRepository('AppBundle:Car')->find($id);
        if ($singleresult === null) {
            $response = [
                'status' => false,
                'message' => "car not found"
            ];
            return new View($response, Response::HTTP_NOT_FOUND);
        }
        return $singleresult;
    }

    /**
     * @Rest\Post("/car")
     */

    public function postAction(Request $request)
    {
        $data = new Car;
        $brand = $request->get('brand');
        $model = $request->get('model');
        if (empty($brand) || empty($model)) {
            $response = [
                'status' => false,
                'message' => "NULL VALUES ARE NOT ALLOWED"
            ];
            return new View($response, Response::HTTP_NOT_ACCEPTABLE);
        }
        $data->setBrand($brand);
        $data->setModel($model);
        $em = $this->getDoctrine()->getManager();
        $em->persist($data);
        $em->flush();
        $response = [
            'status' => false,
            'message' => "Car Added Successfully"
        ];
        return new View($response, Response::HTTP_OK);
    }

    /**
     * @Rest\Put("/car/{id}")
     */
    public function updateAction($id, Request $request)
    {
        $brand = $request->get('brand');
        $model = $request->get('model');
        $sn = $this->getDoctrine()->getManager();
        $car = $this->getDoctrine()->getRepository('AppBundle:Car')->find($id);

        if (empty($car)) {
            $response = [
                'status' => false,
                'message' => "car not found"
            ];
            return new View($response, Response::HTTP_NOT_FOUND);
        } elseif (!empty($brand) && !empty($model)) {
            $car->setBrand($brand);
            $car->setModel($model);
            $sn->flush();
            $response = [
                'status' => false,
                'message' => "Car Updated Successfully"
            ];
            return new View($response, Response::HTTP_OK);
        } elseif (empty($brand) && !empty($model)) {
            $car->setModel($model);
            $sn->flush();
            $response = [
                'status' => false,
                'message' => "Car model Updated Successfully"
            ];
            return new View($response, Response::HTTP_OK);
        } elseif (!empty($brand) && empty($model)) {
            $car->setBrand($brand);
            $sn->flush();
            $response = [
                'status' => false,
                'message' => "Car Brand Updated Successfully"
            ];
            return new View($response, Response::HTTP_OK);
        } else {
            $response = [
                'status' => false,
                'message' => "Car brand or model cannot be empty"
            ];
            return new View($response, Response::HTTP_NOT_ACCEPTABLE);
        }
    }

    /**
     * @Rest\Delete("/car/{id}")
     */
    public function deleteAction($id)
    {
        $sn = $this->getDoctrine()->getManager();
        $car = $this->getDoctrine()->getRepository('AppBundle:Car')->find($id);
        if (empty($car)) {
            $response = [
                'status' => false,
                'message' => "car not found"
            ];
            return new View($response, Response::HTTP_NOT_FOUND);
        } else {
            $sn->remove($car);
            $sn->flush();
        }
        $response = [
            'status' => false,
            'message' => "deleted successfully"
        ];
        return new View($response, Response::HTTP_OK);
    }
}