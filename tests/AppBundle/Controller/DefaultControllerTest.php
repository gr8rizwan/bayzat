<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{
    public function testCreateCar()
    {
        $client = static::createClient();

        $client->request('POST', '/car', [
            'brand' => 'TESt',
            'model' => '444'
        ]);

        echo $client-> getResponse()->getStatusCode();
        $this->assertEquals(200, $client-> getResponse()->getStatusCode());
    }

    public function testGetCars()
    {
        $client = static::createClient();

        $client->request('GET', '/cars');
        echo $client-> getResponse()->getStatusCode();
        $this->assertEquals(200, $client-> getResponse()->getStatusCode());
    }

    public function testDetetCars()
    {
        $client = static::createClient();

        $client->request('GET', '/car/3');
        echo $client-> getResponse()->getStatusCode();
        $this->assertEquals(200, $client-> getResponse()->getStatusCode());
    }

    public function testUpdateCars()
    {
        $client = static::createClient();

        $client->request('PUT', '/car/4', [
            'brand' => 'TESt',
            'model' => '44444333'
        ]);

        echo $client-> getResponse()->getStatusCode();

        $this->assertEquals(200, $client-> getResponse()->getStatusCode());
    }


}
